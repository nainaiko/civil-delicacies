// components/sort-item/sort-item.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        recipe: {
            // 接收数据的类型
            type: Object,
            // value 默认值
            value: {}
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
    }
})
