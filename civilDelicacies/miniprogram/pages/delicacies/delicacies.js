// pages/delicacies/delicacies.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        show: false,
        title: "",
        list: {},
        starlist: [],
        clistId: ""
    },

    changeShow() {
        let that = this
        let showStar = that.data.show
        if (showStar) {
            this.setData({
                show: false
            })
            // 删除该记录
            let cid = this.data.clistId
            this.delCollectionData(cid)

        } else {
            this.setData({
                show: true
            })
            let that = this
            let obj = {
                name: that.data.list.title,
                img: that.data.list.photo
            }
            // 存入
            this.setCollectionData(obj)
        }
    },

    // 获取收藏列表
    getCollectionData(key = ""){
        wx.cloud.callFunction({
            name: 'getCollection'
        }).then(res => {
            let shows = res.result.data
            let haves = shows.filter(i => i.name == key)
            // console.log(haves);
            if(haves[0]){
                this.setData({
                    show: true,
                    clistId: haves[0]._id
                })
            }

            this.setData({
                starlist: shows
            })
        })
        .catch(() => {})
    },
    
    // 添加收藏
    setCollectionData(obj){
        // console.log(obj);
        wx.cloud.callFunction({
            name: 'addCollection',
            data: obj
        }).then((res) => {
            let id = res.result._id
            wx.showToast({
                icon: 'success',
                title: '收藏成功',
            })
            this.setData({
                show: true,
                clistId: id
            })
        }).catch(() => {
            wx.showToast({
                icon: "error",
                title: '收藏失败',
            })
        })
    },

    // 删除收藏
    delCollectionData(cid){
        wx.cloud.callFunction({
            name: 'deleteCollection',
            data: {
                id: cid
            }
        }).then(() => {
            wx.showToast({
                icon: 'success',
                title: '取消收藏成功',
            })
        }).catch(() => {
            wx.showToast({
                icon: "error",
                title: '取消收藏失败',
            })
        })
    },

    // 获取菜谱详情
    getFoodDate(key = "") {
        let dataId = {
            key: key
        }

        wx.showLoading({
            title: '正在查询...',
        })

        wx.cloud.callFunction({
            name: 'getFood',
            data: dataId
        }).then(res => {
            wx.hideLoading()
            let lists = res.result.data[0]
            // console.log(lists);

            this.setData({
                list: lists
            })
        })
        .catch(() => {
            wx.hideLoading()
        })
    },

    // 存储历史记录
    setHistory(t = "") {
        var history = []
        try {
            var value = wx.getStorageSync('history')
            if (value) {
                history = value
            }
        } catch (e) {
            // Do something when catch error
            console.log("存入storage失败");
        }

        let name = t
        if(!history[0]){
            let arr = []
            arr.push(name)
            wx.setStorage({
                key: "history",
                data: arr
            })
        }else{
            if(history.indexOf(name) === -1){
                history.unshift(name)
                wx.setStorage({
                    key: "history",
                    data: history
                })
            }
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            title: options.title
        })
        this.setHistory(options.title)
        this.getFoodDate(options.title)
        this.getCollectionData(options.title)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})