// pages/add_show/add-show.js
const app = getApp()
Page({
    data: {
        userInfo: null,
        fileList: [],
        myValue: "",
        myDesc: ""
    },

    getMyValue(e){
        let value = e.detail.value
        this.setData({
            myValue: value
        });
    },

    getMyDesc(e){
        let value = e.detail.value
        this.setData({
            myDesc: value
        });
    },

    afterRead(event) {
        const { file } = event.detail;
        let file_item = {
            url: file.url,
            isImage: true,
            deletable: false,
            size: file.size
        }
        let files = []
        files.push(file_item)
        this.setData({
            fileList: files
        });
    },

    delImg(){
        this.setData({
            fileList: []
        });
    },

    addShowData() {
        var that = this
        let anm = that.data.userInfo.nickName
        let aimg = that.data.userInfo.avatarUrl
        let url = that.data.fileList[0]?.url
        let mytitle = that.data.myValue.trim()
        let desc = that.data.myDesc.trim()

        if(url && mytitle){
            wx.cloud.callFunction({
                name: 'addShow',
                data: {
                    author: anm,
                    author_img: aimg,
                    img: url,
                    title: mytitle,
                    straight_text: desc,
                    publishtime: "刚刚"
                }
            }).then(() => {
                wx.showToast({
                    icon: 'success',
                    title: '保存成功',
                })
                // 清空表单
                this.setData({
                    fileList: [],
                    myValue: "",
                    myDesc: ""
                })
                // 跳回食秀页面
                wx.switchTab({
                    url: '/pages/show/show'
                })
            }).catch(() => {
                wx.showToast({
                    icon: "error",
                    title: '保存失败',
                })
            })
        }else{
            wx.showToast({
              title: '标题和图片不能为空',
              icon: 'none'
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            userInfo: app.globalData.userInfo
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})