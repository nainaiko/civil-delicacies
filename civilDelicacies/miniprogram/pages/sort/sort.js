// pages/sort/sort.js
Page({
    /**
     * 页面的初始数据
     */
    data: {
        id: "",
        title:"",
        list: []
    },
    getBannerDate(sid = this.id) {
        let dataId = {
            sid:sid
        }
        
        wx.showLoading({
            title: '正在查询...',
        })

        wx.cloud.callFunction({
            name: 'getBanner',
            data: dataId
        }).then(res => {
            wx.hideLoading()
            let lists = res.result.data[0].list
            // console.log(lists);

            this.setData({
                list:lists
            })
        })
        .catch(() => {
            wx.hideLoading()
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            title: options.title,
            id: options.id
        })
        this.getBannerDate(options.id)

        // 页面切换，更换页面标题
        wx.setNavigationBarTitle({
            title: options.title
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
        
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})