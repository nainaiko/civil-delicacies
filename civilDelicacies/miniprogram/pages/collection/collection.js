// pages/collection/collection.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: []
    },

    // 获取收藏列表
    getCollectionData(){
        wx.cloud.callFunction({
            name: 'getCollection'
        }).then(res => {
            let lists = res.result.data
            this.setData({
                list: lists
            })
        })
        .catch(() => {})
    },

    // 删除收藏
    delBtn(e){
        let that = this
        let cid = e.target.dataset.id
        let clist = that.data.list
        let newlist = clist.filter(i => i._id !== cid)
        this.setData({
            list: newlist
        })
        wx.cloud.callFunction({
            name: 'deleteCollection',
            data: {
                id: cid
            }
        }).then(() => {
            wx.showToast({
                icon: 'success',
                title: '取消收藏成功',
            })
        }).catch(() => {
            wx.showToast({
                icon: "error",
                title: '取消收藏失败',
            })
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getCollectionData()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})