// pages/history/history.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: []
    },

    getHistoryList() {
        try {
            var value = wx.getStorageSync('history')
            if (value) {
                this.setData({
                    list: value
                });
            }
        } catch (e) {
            // Do something when catch error
        }
    },

    delHistory(e){
        let name = e.target.dataset.name
        let newlist =this.data.list.filter(i => i !== name)
        console.log(newlist)
        wx.setStorage({
            key: "history",
            data: newlist
        })
        this.setData({
            list: newlist
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getHistoryList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})