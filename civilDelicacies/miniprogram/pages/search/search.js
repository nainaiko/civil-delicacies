// pages/search/search.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        searchValue: "",
        list: [],
        show: false
    },

    getValue(e){
        let value = e.detail.value
        this.setData({
            searchValue: value
        });
    },

    getFoodDate() {
        let that = this
        let key = that.data.searchValue.trim()
        let dataId = {
            key: key
        }
        
        wx.showLoading({
            title: '正在查询...',
        })

        wx.cloud.callFunction({
            name: 'getFood',
            data: dataId
        }).then(res => {
            wx.hideLoading()
            let lists = res.result.data

            this.setData({
                list: lists,
                show: true
            })
        })
        .catch(() => {
            wx.hideLoading()
        })
    },

    searchBtn(){
        let that = this
        let key = that.data.searchValue.trim()
        if(key){
            this.getFoodDate()
        }else{
            wx.showToast({
              title: '关键字不能为空',
              icon: 'none'
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})