// pages/show/show.js
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        show:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getShowData()
    },
    
    login() {
        if(app.globalData.userInfo){
            wx.navigateTo({
                url: '/pages/add_show/add-show'
            })
        }else{
            wx.getUserProfile({
                desc: '获取用户信息',
                success: res => {
                    // console.log(res.userInfo)
                    var user = res.userInfo
                    //设置全局用户信息
                    app.globalData.userInfo = user
                    //设置局部用户信息
                    this.setData({
                        userInfo: user
                    })
    
                    //检查之前是否已经授权登录
                    wx.cloud.database().collection('userInfo').where({
                        _openid: app.globalData.user_openid
                    }).get({
                        success: res => {
                            //原先没有添加，这里添加
                            if (!res.data[0]) {
                                //将数据添加到数据库
                                wx.cloud.database().collection('userInfo').add({
                                    data: {
                                        avatarUrl: user.avatarUrl,
                                        nickName: user.nickName
                                    },
                                    success: res => {
                                        wx.showToast({
                                            title: '登录成功',
                                            icon: 'none'
                                        })
                                        
                                    }
                                })
                            } else {
                                //已经添加过了
                                this.setData({
                                    userInfo: res.data[0]
                                })
                                wx.showToast({
                                    title: '登录成功',
                                    icon: 'none'
                                })
                            }

                            wx.navigateTo({
                                url: '/pages/add_show/add-show'
                            })
                        }
                    })
                }
            })
        }
    },
       
    getShowData() {
        wx.showLoading({
            title: '正在加载...',
        })

        wx.cloud.callFunction({
                name: 'getShow'
            }).then(res => {
                wx.hideLoading()
                let shows = res.result.data
                // console.log(shows);

                this.setData({
                    show: shows
                })
            })
            .catch(() => {
                wx.hideLoading()
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getShowData()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})